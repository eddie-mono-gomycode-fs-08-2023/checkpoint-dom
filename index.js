//defining the anchor <a> element with the class "add-cart"
let carts = document.querySelectorAll('.add-cart');

let products = [
  {
    name: 'Vintage Folded Jeans',
    tag: '003',
    price: 20,
    inCart: 0
  },

  {
    name: 'Men straight Jeans',
    tag: '004',
    price: 25,
    inCart: 0
  },

  {
    name: 'Vintage Folded Jeans',
    tag: '003',
    price: 15,
    inCart: 0
  }
];




for(let i = 0; i < carts.length; i++){
  carts[i].addEventListener('click', () => {
    cartNumbers(products[i]);
    totalCost(products[i]);
  })
  
}

function onLoadCartNumbers(){
  let productNumbers = localStorage.getItem('cartNumbers');
  if(productNumbers){
    document.querySelector('.cart span').textContent = productNumbers;
  }
}


function cartNumbers(products){
  //console.log("you clicked", products);
  let productNumbers = localStorage.getItem('cartNumbers');
  //console.log(productNumbers);
  //console.log(typeof productNumbers);
  productNumbers = parseInt(productNumbers);
  //console.log(typeof productNumbers);
  //set local storage to added items
  //console.log(productNumbers);
  //checking if productNumbers exists in the local storage. Which is true. It is there
  if(productNumbers){   
  localStorage.setItem('cartNumbers', productNumbers + 1);
  document.querySelector('.cart span').textContent = productNumbers +1;
  } else{
    localStorage.setItem('cartNumbers', 1);
    document.querySelector('.cart span').textContent = 1;
  }

setItems(products);

}

function setItems(products){
  let cartItems = localStorage.getItem('productsInCart');
  cartItems = JSON.parse(cartItems);

  if(cartItems != null) {

    if(cartItems[products.tag] == undefined) {
      cartItems = {
        ...cartItems,
        [products.tag]: products
      }
    } 
    
    cartItems[products.tag].inCart += 1;
  } else {   
  products.inCart = 1;
  cartItems = {   
      [products.tag]: products
  }
  }
  
  localStorage.setItem("productsInCart", JSON.stringify(cartItems));
}

function totalCost(products){
  //console.log('The product price is', products.price);
  let cartCost = localStorage.getItem('totalCost');
  
  
  //console.log('my cartCost is', cartCost);
  //console.log(typeof cartCost);
  if(cartCost != null){
    cartCost = parseInt(cartCost);
    localStorage.setItem('totalCost', cartCost + products.price);
  }else {
    localStorage.setItem('totalCost', products.price);};
}

function displayCart(){    
let cartItems = localStorage.getItem('productsInCart');
cartItems = JSON.parse(cartItems);
//console.log(cartItems);
let productsContainer = document.querySelector('.products-container');
let cartCost = localStorage.getItem('totalCost');
if(cartItems && productsContainer != null){
  //console.log('running');
  productsContainer.innerHTML = ' ';
  Object.values(cartItems).map(item => {
    /*I add a back tick to join the elements together*/
    productsContainer.innerHTML += `   
    <div class = "products">
    <i class="fa-solid fa-circle-xmark"></i>
    <img src="./images/${item.tag}.jpg">
    <span>${item.name}</span></div>
        </div>
        <div class = "price">$${item.price},00</div>
        <div class = "quantity">
        
        <i class="fa-solid fa-caret-left"></i>
        <span>${item.inCart}</span>
        
        <i class="fa-solid fa-caret-right"></i>
        </div>
        <div class = "total">
        $${item.inCart * item.price},00
        </div>
        `;

    
  });

  productsContainer.innerHTML += `
  <div class = "basketTotalContainer">
  <h4 class = "basketTotalTitle">Basket Total</div>
  <h4 class = "basketTotal">$${cartCost},00</div>

  `

}
}


onLoadCartNumbers();
displayCart();